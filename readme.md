# feta.py
### A data analysis script for .xls I-V files

This is a simple script for analysing outputs produced by the 4200 SCS Keithley semiconductor characterisation system. 
## Usage

To keep the usage of the script a simple as possible the script relies on a consistent naming scheme for the data to be analysed. By calling:
```bash
$python feta.py <name_of_sample>
```
The script can analyse data gathered using either res2t or 4SMU using the Keithley 4200. In the case of res2t the pad data is in separate directories while in the case of the 4SMU they are all saved in 2 .xls documents. It is not required to specify which of the two is used. 
The script will look for the directory `<name_of_sample>/` inside of which it is either expecting to find directories named `<name_of_sample>/Pad<i>/` each containing two files `<name_of_sample>/Pad<i>/CO1to16.xls` and	`<name_of_sample>/Pad<i>/CO16to32.xls` (for res2t), or two files containing all of the pad data at `<name_of_sample>/CO1to16.xls` and	`<name_of_sample>/CO16to32.xls` (for 4SMU). 
The script reads these files extracting the data, doing some basic data analysis, producing plots and .csv files of the data in the parent directory with name `<name_of_sample>_PAD<number>/<name_of_sample>_<field contained>.csv`.
To analyse batches of samples, you can just run
```bash
$python feta.py <name_of_sample1> <name_of_sample2> <...>
```
which will apply the same operation to each sample.

## Open issues 
* Windows support
* Support for a `-merge` option to merge connecting data
