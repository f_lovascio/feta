#!/usr/bin/env python
# coding: utf-8

#System libs
import os
import sys
#Standard libs
import pandas as pd
import numpy as np
#User imports
from fetaPlot import *
from fetAnalysis import *



#Just plotting stuff



Samples = sys.argv[1:]
#print(Samples)

for sample in Samples:
    Dat = DataGather(sample)
    PlotItAll_V2(Dat, sample)
