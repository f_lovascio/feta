import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
if os.name == 'nt':
    print("Windows is not currently supported")
else:
    from fetaReadNix import *

def MakeCSV(off,on,delI,delIoI,FileName):
    Dir=FileName+"_CSVs/"
    if not os.path.exists(Dir):
        os.makedirs(Dir)
    with open(os.path.join(Dir, FileName+"_off.csv"), 'w'):
        pass
    with open(os.path.join(Dir, FileName+"_on.csv"), 'w'):
        pass
    with open(os.path.join(Dir, FileName+"_DI.csv"), 'w'):
        pass
    with open(os.path.join(Dir, FileName+"_DIoI.csv"), 'w'):
        pass
    off.to_csv(Dir+"/"+FileName+"_off.csv",index=False)
    on.to_csv(Dir+"/"+FileName+"_on.csv",index=False)
    delI.to_csv(Dir+"/"+FileName+"_DI.csv",index=False)
    delIoI.to_csv(Dir+"/"+FileName+"_DIoI.csv",index=False)


def DeltaI(on,off):
    AIColumns=filter(lambda colnames: 'I' in colnames, on.columns.values.tolist())
    Delta_Is = pd.DataFrame()
    Delta_Is['V'] = off['AV']
    for column in AIColumns:
        Delta_Is["|Δ"+column+"|"]=on[column]-off[column]
    return Delta_Is

def unbaisedDeltaI(on,off):
    dfUBDI=pd.DataFrame()
    dfUBDI['V']=off['AV']
    for i in range(0,15):
        dfUBDI[str(i)+'DI']=on['I '+str(i)]-0.5*(off['I '+str(i+1)]+off['I '+str(i)])
    dfUBDI['Mean']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).mean(axis=1)
    dfUBDI['Std Deviation']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).std(axis=1)
    dfUBDI['Std Error']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).std(axis=1)*0.2582
    return dfUBDI

def unbaisedDeltaI_HO(on,off):
    dfUBDI=pd.DataFrame()
    dfUBDI['V']=off['AV']
    offInt=pd.DataFrame()
    for i in range(0,16):
        offInt[2*i]=off['I '+str(i)]
        offInt[(2*i)+1]=np.nan
    offInt=offInt.interpolate(axis=1,method='spline',order=3)
#    plo=plt.figure()
#    plt.plot(np.array(offInt.iloc[[2]])[0,:],'ro')
#    plt.show()
    for i in range(0,16):
        dfUBDI[str(i)+'DI']=on['I '+str(i)]-offInt[(2*i)+1]
    dfUBDI['Mean']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).mean(axis=1)
    dfUBDI['Std Deviation']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).std(axis=1)
    dfUBDI['Std Error']=(dfUBDI.filter(filter(lambda colnames: 'DI' in colnames, dfUBDI.columns.values.tolist()), axis=1)).std(axis=1)*0.2582
    return dfUBDI

def AddStats(df):
    df1=df
    df1['Mean']=(df.filter(filter(lambda colnames: 'I' in colnames, df.columns.values.tolist()), axis=1)).mean(axis=1)
    df1['Std Deviation']=(df.filter(filter(lambda colnames: 'I' in colnames, df.columns.values.tolist()), axis=1)).std(axis=1)
    df1['Std Error']=(df.filter(filter(lambda colnames: 'I' in colnames, df.columns.values.tolist()), axis=1)).std(axis=1)*0.25
    return df1

def DeltaI2(on,off,DI):
    DI['DI2']=abs(on['Mean']-off['Mean'])
    DI['Std2']=0.25*np.sqrt(on['Std Deviation']*on['Std Deviation']+off['Std Deviation']*off['Std Deviation'])
    return DI

def DelIStats(DI):
    DI['% Error']=100.0*DI['Std Error']/DI['Mean']
    DI['SNR']=DI['Mean']/DI['Std Error']
    return DI

def DeltaIOverI(DI,Off):
    OffColumns=filter(lambda colnames: 'I' in colnames, Off.columns.values.tolist())
    DIOI = pd.DataFrame()
    DIOI['V'] = Off['AV']
    for column in OffColumns:
        DIOI["|Δ"+column+"|/"+column]=abs(DI["|Δ"+column+"|"]/Off[column])
    return DIOI

def DoItAll(DirectoryName,PadNumber):
    filename1=DirectoryName+"/"+"PAD"+str(PadNumber)+"/"+"CO1to16.xls"
    filename2=DirectoryName+"/"+"PAD"+str(PadNumber)+"/"+"CO16to32.xls"
    Off,On=ReadXCelltoDf(filename1,filename2)
    DIs=DelIStats(AddStats(DeltaI(On,Off)))
    Off=AddStats(Off)
    On=AddStats(On)
    DIoverI=AddStats(DeltaIOverI(DIs,Off))
    DIs=DeltaI2(On,Off,DIs)
    uDIs=unbaisedDeltaI_HO(On,Off)
    try:
        MakeCSV(Off,On,DIs,DIoverI,DirectoryName+'_Pad'+str(PadNumber))
    except:
        print("failed to make CSVs")
    print('Done! :)')
    return  {"Off": Off,"On":On,"DI":DIs,"DI/I":DIoverI,"uDI":uDIs}


def DataGather(SampleName):
    Pads={}
    print("Looking for a multipad file")
    if os.path.isfile(SampleName+"/CO1to16.xls") and os.path.isfile(SampleName+"/CO16to32.xls"):
        Pads=ReadXCelltoDf_Multipad(SampleName+"/CO1to16.xls", SampleName+"/CO16to32.xls")
        for Pad,Data in Pads.items():
            Data.update({'DI':DelIStats(AddStats(DeltaI(Data['On'],Data['Off'])))})
            Data.update({'DI/I':AddStats(DeltaIOverI(Data['DI'],Data['Off']))})
            Data['Off']=AddStats(Data['Off'])
            Data['On']=AddStats(Data['On'])
            Data['uDI']=unbaisedDeltaI_HO(Data['On'],Data['Off'])
            Data['DI']=DeltaI2(Data['On'],Data['Off'],Data['DI'])
            MakeCSV(Data["Off"],Data["On"],Data["DI"],Data["DI/I"],SampleName+'_'+Pad)
    else:
        print("No multipad file, looking for pad folders instead")
        for i in range(1,5):
            pad='Pad'+str(i)
            #PAD='PAD'+str(i)
            try:
                print("Trying "+pad+" data")
                Data=DoItAll(SampleName,i)
                Pads.update({pad:Data})
            except:
                print("no "+pad+" data")
    return Pads


