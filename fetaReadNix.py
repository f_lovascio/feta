import pandas as pd
import numpy as np
import sys

def ReadSingleXCelltoDf(name):
    #read a single spreadsheet
    Xcell=pd.ExcelFile(name)

    dfOff=Xcell.parse('Data').filter(['Time','AV','AI'], axis=1)

    dfOn=Xcell.parse('Append1').filter(['Time','AV','AI'], axis=1)

    for N,sheetName in enumerate(['Append'+str(i) for i in range(2,16,2)]):
        dfOff['I '+str(N+1)]=Xcell.parse(sheetName)['AI']
    for N,sheetName in enumerate(['Append'+str(i) for i in range(3,16,2)]):
        dfOn['I '+str(N+1)]=Xcell.parse(sheetName)['AI']
    return dfOff,dfOn

def ReadXCelltoDf(name1, name2):
    #read two connecting spreadsheets

    #load first file
    Xcell=pd.ExcelFile(name1)

    #create the dataframes with the first sheets in the file
    dfOff=Xcell.parse('Data').filter(['Time','AV'], axis=1)
    dfOff['I 0']=Xcell.parse('Data').filter(['AI'], axis=1)
    dfOn=Xcell.parse('Append1').filter(['Time','AV'], axis=1)
    dfOn['I 0']=Xcell.parse('Append1').filter(['AI'], axis=1)

    #populate the dataframes
    for N,sheetName in enumerate(['Append'+str(i) for i in range(2,16,2)]):
        dfOff['I '+str(N+1)]=Xcell.parse(sheetName).filter(['AI'], axis=1)
    for N,sheetName in enumerate(['Append'+str(i) for i in range(3,16,2)]):
        dfOn['I '+str(N+1)]=Xcell.parse(sheetName).filter(['AI'], axis=1)

    #load second file
    Xcell=pd.ExcelFile(name2)

    dfOff['I 8']=Xcell.parse('Data').filter(['AI'], axis=1)
    dfOn['I 8']=Xcell.parse('Append1').filter(['AI'], axis=1)['AI']

    for N,sheetName in enumerate(['Append'+str(i) for i in range(2,16,2)]):
        dfOff['I '+str(N+9)]=Xcell.parse(sheetName).filter(['AI'], axis=1)
    for N,sheetName in enumerate(['Append'+str(i) for i in range(3,16,2)]):
        dfOn['I '+str(N+9)]=Xcell.parse(sheetName).filter(['AI'], axis=1)

    return dfOff,dfOn

def ReadXCelltoDf_Multipad(name1, name2):
    #read two connecting spreadsheets with multiple pad data
    Xcell1=pd.ExcelFile(name1)
    Xcell2=pd.ExcelFile(name2)

    #create the dataframes with the first sheets in the file
    Npads=0
    COL_LIST=["PAD1I","PAD2I","PAD3I","PAD4I"]
    df1=Xcell1.parse('Data')
    for col in COL_LIST:
        if col in df1.columns:
            Npads +=1
    Pads={}
    for column in COL_LIST[0:Npads]:
        dfOff=df1.filter(['Time'], axis=1)
        dfOff['AV']=df1.filter(['PAD1V'], axis=1)
        dfOff['I 0']=df1.filter([column], axis=1)
        dfOn=Xcell1.parse('Append1').filter(['Time'], axis=1)
        dfOn['AV']=Xcell1.parse('Append1').filter(['PAD1V'], axis=1)
        dfOn['I 0']=Xcell1.parse('Append1').filter([column], axis=1)

        for N,sheetName in enumerate(['Append'+str(i) for i in range(2,16,2)]):
            dfOff['I '+str(N+1)]=Xcell1.parse(sheetName).filter([column], axis=1)
        for N,sheetName in enumerate(['Append'+str(i) for i in range(3,16,2)]):
            dfOn['I '+str(N+1)]=Xcell1.parse(sheetName).filter([column], axis=1)

        dfOff['I 8']=Xcell2.parse('Data').filter([column], axis=1)
        dfOn['I 8']=Xcell2.parse('Append1').filter([column], axis=1)[column]

        for N,sheetName in enumerate(['Append'+str(i) for i in range(2,16,2)]):
            dfOff['I '+str(N+9)]=Xcell2.parse(sheetName).filter([column], axis=1)
        for N,sheetName in enumerate(['Append'+str(i) for i in range(3,16,2)]):
            dfOn['I '+str(N+9)]=Xcell2.parse(sheetName).filter([column], axis=1)

        Pad={'Off':dfOff,'On':dfOn}
        Pads.update({column[:-1].title():Pad})
    return Pads

