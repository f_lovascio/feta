import matplotlib.pyplot as plt
import os
import itertools

def PlotItAll_V2(Data,SampleName):
    marker = ['o', 'D', '^', 'x'] 
    Dir=SampleName+"_Plots/"
    if not os.path.exists(Dir):
        os.makedirs(Dir)
    with open(os.path.join(Dir, 'Off.pdf'), 'w'):
        pass
    with open(os.path.join(Dir, 'On.pdf'), 'w'):
        pass
    with open(os.path.join(Dir, 'DIs.pdf'), 'w'):
        pass
    with open(os.path.join(Dir, 'PercentError.pdf'), 'w'):
        pass
    with open(os.path.join(Dir, 'SNR.pdf'), 'w'):
        pass
    with open(os.path.join(Dir, 'DIOI.pdf'), 'w'):
        pass

    marker=itertools.cycle(marker[0:len(Data)])

    Plot1=plt.figure()
    plt.yscale('log')
    plt.ylabel("|I| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['Off']['AV'],abs(dat['Off']['Mean']),yerr=dat['Off']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/Off.pdf')

    Plot2=plt.figure()
    plt.yscale('log')
    plt.ylabel("|I| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['On']['AV'],abs(dat['On']['Mean']),yerr=dat['On']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/On.pdf')

    Plot3=plt.figure()
    plt.yscale('log')
    plt.ylabel("|$\Delta$I| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['Off']['AV'],abs(dat['DI']['Mean']),yerr=dat['DI']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/DIs.pdf')

    Plot7=plt.figure()
    plt.yscale('log')
    plt.ylabel("|$\Delta$I v2| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['Off']['AV'],abs(dat['DI']['DI2']),yerr=dat['DI']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/DI2s.pdf')

    Plot8=plt.figure()
    plt.ylabel("|$\Delta$I v2| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['Off']['AV'],abs(dat['DI']['DI2']),yerr=dat['DI']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/DI2nolog.pdf')

    Plot9=plt.figure()
    plt.yscale('log')
    plt.ylabel("|unbaised $\Delta$I| (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['uDI']['V'],abs(dat['uDI']['Mean']),yerr=dat['uDI']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/uDI.pdf')

    Plot10=plt.figure()
    plt.ylabel("unbaised $\Delta$I (A)")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['uDI']['V'],dat['uDI']['Mean'],yerr=dat['uDI']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/uDI.pdf')

    Plot4=plt.figure()
    plt.ylabel("Percentage Error")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.plot(dat['DI']['V'],abs(dat['DI']['% Error']),marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/PercentError.pdf')

    Plot5=plt.figure()
    plt.ylabel("SNR")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.plot(dat['DI']['V'],abs(dat['DI']['SNR']),marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/SNR.pdf')

    Plot6=plt.figure()
    plt.yscale('log')
    plt.ylabel("|$\Delta$I|/I")
    plt.xlabel("V (V)")
    for Pad,dat in Data.items():
        plt.errorbar(dat['DI/I']['V'],abs(dat['DI/I']['Mean']),yerr=dat['DI/I']['Std Error'],solid_capstyle='round',marker=next(marker),ms=4,ls='none',label=Pad)
    plt.legend()
    plt.savefig(SampleName+'_Plots/DIOI.pdf')

    #plt.show()

    return Plot1, Plot2, Plot3, Plot4, Plot5, Plot6, Plot7, Plot8, Plot9

def PlotItAll(Data,SampleName):

    Off=Data["Off"]
    On=Data["On"]
    DIs=Data["DI"]
    DIoIs=Data["DI/I"]

    Plot1=plt.figure()
    plt.yscale('log')
    plt.ylabel("|I| (A)")
    plt.xlabel("V (V)")
    plt.errorbar(Off['AV'],abs(Off['Mean']),yerr=Off['Std Error'],solid_capstyle='round',marker='o',ms=5,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/Off.pdf')

    Plot2=plt.figure()
    plt.yscale('log')
    plt.ylabel("|I| (A)")
    plt.xlabel("V (V)")
    plt.errorbar(On['AV'],abs(On['Mean']),yerr=On['Std Error'],solid_capstyle='round',marker='o',ms=5,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/On.pdf')

    Plot3=plt.figure()
    plt.yscale('log')
    plt.ylabel("|ΔI| (A)")
    plt.xlabel("V (V)")
    plt.errorbar(Off['AV'],abs(DIs['Mean']),yerr=DIs['Std Error'],solid_capstyle='round',marker='o',ms=4,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/DIs.pdf')

    Plot4=plt.figure()
    plt.ylabel("Percentage Error")
    plt.xlabel("V (V)")
    plt.plot(Off['AV'],abs(DIs['% Error']),marker='o',ms=4,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/PercentError.pdf')

    Plot5=plt.figure()
    plt.ylabel("SNR")
    plt.xlabel("V (V)")
    plt.plot(Off['AV'],abs(DIs['SNR']),marker='o',ms=4,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/SNR.pdf')

    Plot6=plt.figure()
    plt.yscale('log')
    plt.ylabel("|ΔI|/I")
    plt.xlabel("V (V)")
    plt.errorbar(Off['AV'],abs(DIoIs['Mean']),yerr=DIoIs['Std Error'],solid_capstyle='round',marker='o',ms=4,ls='none',c='red',label="Pad 3")
    plt.legend()
    plt.savefig(SampleName+'_Plots/DIOI.pdf')

    return Plot1, Plot2, Plot3, Plot4, Plot5, Plot6

